Description
===

Build a API for Persons

Requirements
===

Use node version

```
nvm use
```

Install dependencies

```
npm install
```

Running MySQL using Docker

```
docker-compose up -d
```
